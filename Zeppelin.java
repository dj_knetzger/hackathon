
public class Zeppelin extends Unit{

	private Unit cargo;
	
	public Zeppelin(Player owner, Tile startTile){
		super(UnitType.ZEPPELIN,owner,startTile);
		cargo = null;
	}

	public void load(Unit unitToLoad){
		if(cargo==null){
			cargo = unitToLoad;
			unitToLoad.isLoaded = true;
		}
	}

	public boolean isLoaded(){
		return cargo!=null;
	}

	public Unit getLoadedUnit(){
		return cargo;
	}

	public Unit drop(){
		Unit temp = cargo;
		temp.isLoaded = false;
		cargo = null;
		return temp;
	}
}