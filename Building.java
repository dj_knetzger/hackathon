import javafx.scene.image.Image;

public class Building{

	public BuildingType type;
	public Player owner;
	public int hp;

	public boolean hasBuiltThisTurn;

	public Building(BuildingType type, Player owner){
		this.type = type;
		this.owner = owner;
		this.hp = type.captureHP;

		owner.addBuilding(this);

		hasBuiltThisTurn = type == BuildingType.CITY;
	}

	public boolean captureBuilding(Unit unit) {
		this.hp -= unit.getCapturePower();
		if (this.hp <= 0) {
			if(this.owner != null) {
				this.owner.removeBuilding(this);
			}
			unit.owner.addBuilding(this);
			resetCaptureHP();
			return true;
		}
		return false;
	}

	public void resetCaptureHP(){
		this.hp = type.captureHP;
	}

	public Image getImage(){
		return type.getImage(owner);
	}
}