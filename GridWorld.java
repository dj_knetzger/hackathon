import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class GridWorld {
	
	private GridPane root;
	private String name;
	private int x;
	private int y;
	private Tile[][] matrix;
	public Tile selectedTile = null;
	public ArrayList<Tile> targetTiles = new ArrayList<Tile>();
	public Tile focusedTile;

	private Player redPlayer;
	private Player bluePlayer;
	
	public boolean moveflag = false;
    public boolean attackflag = false;
    public boolean loadFlag = false;
	public boolean dropFlag = false;

	private Player currentPlayer;
	private Stage stageWindow;
	
	public ArrayList<Tile> buildings = new ArrayList<Tile>();

	public static GridWorld gridWorld;

	public GridWorld(GridPane root, int x, int y, Stage stageWindow) {
		this.root = root;
		this.matrix = new Tile[x][y];
		this.name = name;
		this.x = x;
		this.y = y;
		this.stageWindow = stageWindow;
		
		redPlayer = new Player(Player.Team.RED);
		bluePlayer = new Player(Player.Team.BLUE);

		currentPlayer = bluePlayer;
		stageWindow.setTitle("It is now Blue's turn. Cash: " + bluePlayer.money);

		GridWorld.gridWorld = this;
	}
	
	public void paintMap(String file) throws IOException{
		File dir = new File(System.getProperty("user.dir"));
		File map = new File(dir,file);
		BufferedReader in = new BufferedReader(new FileReader(map)); 
		//Header of the file
		String text = in.readLine(); 
		for(int mapy = 0; mapy < y; mapy++) { 
			text = in.readLine(); 
			char[] ctext = text.toCharArray();
			  
			for(int mapx = 0; mapx < x; mapx++){
				ImageView imageview = new ImageView();
				Terrain t;
				Building b = null;
				Player buildingOwner = redPlayer;
				boolean save = false;
				  
				switch (ctext[mapx]){
				 	case 'W':
				 		t = Terrain.WATER;
				 		break;
				 	case 'P': 
				 		t = Terrain.FIELD;
				 		break;
				    case 'F':
				 		t = Terrain.FOREST;
				    	break;
				    case 'M':
				 		t = Terrain.MOUNTAIN;
				    	break;
				    case 'R':
				 		t = Terrain.ROAD;
				 		break;
				    case 'C':
				    	buildingOwner = bluePlayer;
				    case 'c':
				    	t = Terrain.CITY;
				    	b = new Building(BuildingType.CITY, buildingOwner);
				    	t.owner = buildingOwner;
				    	save = true;
				    	break;
				    case 'B':
				    	buildingOwner = bluePlayer;
				    case 'b':
				    	t = Terrain.BARRACKS;
				    	b = new Building(BuildingType.BARRACKS, buildingOwner);
				    	t.owner = buildingOwner;
				    	save = true;
				    	break;
				    case 'A':
				    	buildingOwner = bluePlayer;
				    case 'a':
				    	t = Terrain.AVIARY;
				    	b = new Building(BuildingType.AVIARY, buildingOwner);
				    	t.owner = buildingOwner;
				    	save = true;
				    	break;
				    case 'S':
				    	buildingOwner = bluePlayer;
				    case 's':
				    	t = Terrain.PORT;
				    	b = new Building(BuildingType.PORT, buildingOwner);
				    	t.owner = buildingOwner;
				    	save = true;
				    	break;
				    default:
				    	t = null;
				}
				if((file.equals("res/maps/RiverMap.txt"))&&(mapx==10&&mapy==6)||(mapx==18&&mapy==1)){
					buildingOwner.removeBuilding(b);
					b.owner = null;
				}
			 	matrix[mapx][mapy] = new Tile(mapx,mapy,t,b);
		    	GridPane.setConstraints(matrix[mapx][mapy], mapx, mapy);
		    	root.getChildren().add(matrix[mapx][mapy]);
		    	if(save){buildings.add(matrix[mapx][mapy]);};
		    	matrix[mapx][mapy].setOnMouseClicked(new EventHandler<MouseEvent>(){

					@Override
					public void handle(MouseEvent e) {
						handleMouseClick(e);
					}
			    	
			    });
		    }
		}

		in.close();
	}

	public ArrayList<Tile> getLoadableTiles(Tile tile){
		Tile center = tile;
		ArrayList<Tile> visited = new ArrayList<Tile>();
		Queue<Tile> toVisit = new LinkedList<Tile>();
		Queue<Integer> toVisitDist = new LinkedList<Integer>();
		ArrayList<Tile> hasAccess = new ArrayList<Tile>();
		if(tile.unit.type.movementType != Unit.MovementType.FOOT){
			return hasAccess;
		}
		toVisit.add(tile);
		toVisitDist.add(2);
		
		Player.Team team = center.unit.team;
		
		while(!toVisit.isEmpty()){
			Tile nextTile = toVisit.remove();
			Integer dist = toVisitDist.remove();
			if(!visited.contains(nextTile)) {
				visited.add(nextTile);
				if(dist > 0){
					if(nextTile.unit != null && nextTile.unit.team == team && nextTile.unit.type==UnitType.ZEPPELIN) {
						hasAccess.add(nextTile);
					}
					if(nextTile.x != 0){
						toVisit.add(matrix[nextTile.x-1][nextTile.y]);
						toVisitDist.add(dist-1);
					}
					if(nextTile.x != this.x-1){
						toVisit.add(matrix[nextTile.x+1][nextTile.y]);
						toVisitDist.add(dist-1);
					}
					if(nextTile.y != 0){
						toVisit.add(matrix[nextTile.x][nextTile.y-1]);
						toVisitDist.add(dist-1);
					}
					if(nextTile.y != this.y-1){
						toVisit.add(matrix[nextTile.x][nextTile.y+1]);
						toVisitDist.add(dist-1);
					}		
				}
			}
		}
		return hasAccess;
	}
	
	public ArrayList<Tile> getTargetableTiles(Tile tile){
		Tile center = tile;
		ArrayList<Tile> visited = new ArrayList<Tile>();
		Queue<Tile> toVisit = new LinkedList<Tile>();
		Queue<Integer> toVisitDist = new LinkedList<Integer>();
		ArrayList<Tile> hasAccess = new ArrayList<Tile>();
		toVisit.add(tile);
		toVisitDist.add(tile.unit.type.maxRange+1);
		
		Player.Team team = center.unit.team;
		
		while(!toVisit.isEmpty()){
			Tile nextTile = toVisit.remove();
			Integer dist = toVisitDist.remove();
			if(!visited.contains(nextTile)) {
				visited.add(nextTile);
				if(dist > 0){
					if(nextTile.unit != null && nextTile.unit.team != team && tile.unit.canAttack(nextTile.unit)) {
						hasAccess.add(nextTile);
					}
					if(nextTile.x != 0){
						toVisit.add(matrix[nextTile.x-1][nextTile.y]);
						toVisitDist.add(dist-1);
					}
					if(nextTile.x != this.x-1){
						toVisit.add(matrix[nextTile.x+1][nextTile.y]);
						toVisitDist.add(dist-1);
					}
					if(nextTile.y != 0){
						toVisit.add(matrix[nextTile.x][nextTile.y-1]);
						toVisitDist.add(dist-1);
					}
					if(nextTile.y != this.y-1){
						toVisit.add(matrix[nextTile.x][nextTile.y+1]);
						toVisitDist.add(dist-1);
					}		
				}
			}
		}
		return hasAccess;
	}
	
	public ArrayList<Tile> getAccessibleTiles(Tile tile, Unit unit){
		Tile center = tile;
		if(unit == null){
			unit = tile.unit;
		}
		ArrayList<Tile> visited = new ArrayList<Tile>();
		Queue<Tile> toVisit = new LinkedList<Tile>();
		Queue<Double> toVisitDist = new LinkedList<Double>();
		ArrayList<Tile> hasAccess = new ArrayList<Tile>();
		toVisit.add(tile);
		if(dropFlag){
			toVisitDist.add((double)2);
		}else{
			toVisitDist.add((double)tile.unit.type.speed+1.);
		}

		Player.Team moveTeam = unit.team;
		
		while(!toVisit.isEmpty()){
			Tile nextTile = toVisit.remove();
			Double dist = toVisitDist.remove();
			if(!visited.contains(nextTile) && isLegalTile(nextTile.terrain, unit)) {
				visited.add(nextTile);
				if(dist > 0){
					if(nextTile.unit == null) {
						hasAccess.add(nextTile);
					}
					if(!(nextTile.unit != null && nextTile.unit.team != moveTeam)){
						double moveMod;
						if (unit.type.movementType == Unit.MovementType.FLYING){
							moveMod = 1;
						} else {
							moveMod = matrix[nextTile.x][nextTile.y].terrain.movementModifier;
						}
						if(nextTile.x != 0){
							toVisit.add(matrix[nextTile.x-1][nextTile.y]);
							toVisitDist.add(dist-1/moveMod);
						}
						if(nextTile.x != this.x-1){
							toVisit.add(matrix[nextTile.x+1][nextTile.y]);
							toVisitDist.add(dist-1/moveMod);
						}
						if(nextTile.y != 0){
							toVisit.add(matrix[nextTile.x][nextTile.y-1]);
							toVisitDist.add(dist-1/moveMod);
						}
						if(nextTile.y != this.y-1){
							toVisit.add(matrix[nextTile.x][nextTile.y+1]);
							toVisitDist.add(dist-1/moveMod);
						}
					}		
				}
			}
		}
		hasAccess.remove(tile);
		return hasAccess;
	}
	
	public void handleMouseClick(MouseEvent e){
		Tile tile = (Tile)e.getSource();
		selectedTile = tile;

		// Bring up the building menu
		if (focusedTile == null && tile.unit == null && tile.terrain.isBuilding() && tile.building != null && tile.building.owner != null && tile.building.owner.team == currentPlayer.team && !moveflag && !dropFlag) {
			BuildingMenu menu = new BuildingMenu(selectedTile, currentPlayer);
		}
		// Select a unit
		else if (focusedTile == null && tile.unit != null && tile.unit.team == currentPlayer.team && !tile.unit.hasAttackedThisTurn) {
			focusedTile = tile;
			tile.setSelected();
		}
		// Move the selected unit
		else if (focusedTile != null && targetTiles.contains(tile) && moveflag) {
			focusedTile.unit.move(tile);
			clearFocusedTile();
			if(!tile.unit.hasAttackedThisTurn){
				focusedTile = tile;
				tile.setSelected();
			}
			moveflag = false;
		}
		// Drop units from a zeppelin
		else if (focusedTile != null && tile.unit == null && targetTiles.contains(tile) && dropFlag){
			if(focusedTile.unit.type == UnitType.ZEPPELIN){
				Zeppelin zUnit = (Zeppelin)focusedTile.unit;
				tile.addUnit(zUnit.drop());
				tile.unit.hasMovedThisTurn = true;
				tile.unit.hasAttackedThisTurn = true;
				zUnit.hasMovedThisTurn = true;
				zUnit.hasAttackedThisTurn = true;
				clearFocusedTile();
			}
		}
		// Selected another unit after the intial select
		else if (focusedTile != null && tile.unit != null && attackflag) {
			if(focusedTile.unit.canAttack(tile.unit)){
				boolean didDie = focusedTile.unit.attack(tile.unit,tile.terrain);
				if(didDie){
					tile.removeUnit();
				}
				clearFocusedTile();
				attackflag = false;
			}else{

			}
		// Load units into a zeppelin
		}else if (focusedTile != null && tile.unit != null && loadFlag) {
			if(tile.unit.team == currentPlayer.team && tile.unit.type == UnitType.ZEPPELIN && focusedTile.unit.type.movementType == Unit.MovementType.FOOT){
				Zeppelin zUnit = (Zeppelin)tile.unit;
				if(!zUnit.isLoaded()){
					zUnit.load(focusedTile.unit);
					focusedTile.unit.hasMovedThisTurn = true;
					focusedTile.unit.hasAttackedThisTurn = true;
					focusedTile.removeUnit();
					clearFocusedTile();
				}
			}
		}
		//clear selection/do nothing
		else{
			clearFocusedTile();
			moveflag = false;
			attackflag = false;
			loadFlag = false;
			dropFlag = false;
		}

		checkTurnEnd();
	}

	public void checkTurnEnd(){
		if(currentPlayer.isTurnDone()){
			endTurn();
		}
		if(isGameOver()){
			new EndGameWindow();
		}
	}

	public void endTurn(){
		currentPlayer.endTurn();
		switch(currentPlayer.team){
			case RED:
				currentPlayer = bluePlayer;
				stageWindow.setTitle("It is Blue's Turn. Cash: "+ bluePlayer.money);
				break;
			case BLUE:
				currentPlayer = redPlayer;
				stageWindow.setTitle("It is Red's Turn. Cash: "+ redPlayer.money);
				break;
			default:
				break;
		}
	}

	public boolean isGameOver(){
		return !redPlayer.hasUnits || !bluePlayer.hasUnits;
	}

	void clearFocusedTile(){
		for(Tile targetTile:targetTiles){
			targetTile.clearMask();
		}
		targetTiles = new ArrayList<Tile>();
		if(focusedTile != null){
			focusedTile.clearSelected();
		}
		focusedTile = null;
	}

	private boolean isLegalTile(Terrain terrain, Unit unitToCheck) {

		return terrain.isLegalTerrain(unitToCheck);

		// if (unitToCheck.type.travelMedium == Unit.TravelMedium.AIR || unitToCheck.type.travelMedium == Unit.TravelMedium.DRAGON) {
		// 	return true;
		// }
		// else if (terrain == Terrain.WATER && unitToCheck.type.travelMedium != Unit.TravelMedium.SEA) {
		// 	return false;
		// }
		// else if (terrain != Terrain.WATER && unitToCheck.type.travelMedium == Unit.TravelMedium.SEA) {
		// 	return false;
		// }
		// else if (terrain == Terrain.MOUNTAIN && unitToCheck.type.movementType == Unit.MovementType.WHEEL) {
		// 	return false;
		// }
		// else {
		// 	return  true;
		// }
	}

	/**
	* Mother I'm sorry
	*/
	public static void checkTurnEndStatically(){
		GridWorld.gridWorld.checkTurnEnd();
	}
}
