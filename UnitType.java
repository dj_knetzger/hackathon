import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.image.Image;

public enum UnitType{
	SCOUT(Unit.TravelMedium.LAND, 10, false, new Unit.TravelMedium[]{Unit.TravelMedium.LAND, Unit.TravelMedium.DRAGON}, 4, Unit.DamageType.BLADE, 1, 1, 2, Unit.ArmorType.LEATHER, 5, Unit.MovementType.FOOT, true, 3, 99, 99),
	SWORDSMEN(Unit.TravelMedium.LAND, 25, false, new Unit.TravelMedium[]{Unit.TravelMedium.LAND, Unit.TravelMedium.DRAGON}, 6, Unit.DamageType.BLADE, 1, 1, 4, Unit.ArmorType.LEATHER, 2, Unit.MovementType.FOOT, true, 2, 99, 99),
	KNIGHT(Unit.TravelMedium.LAND, 80, false, new Unit.TravelMedium[]{Unit.TravelMedium.LAND, Unit.TravelMedium.DRAGON}, 8, Unit.DamageType.BLADE, 1, 1, 6, Unit.ArmorType.PLATE, 2, Unit.MovementType.HORSE, false, 1, 50, 99),
	MAGE(Unit.TravelMedium.LAND,  45, true, new Unit.TravelMedium[]{Unit.TravelMedium.LAND,Unit.TravelMedium.SEA,Unit.TravelMedium.AIR, Unit.TravelMedium.DRAGON}, 6, Unit.DamageType.MAGIC, 2, 3, 1, Unit.ArmorType.NONE, 3, Unit.MovementType.FOOT, false, 2, 50, 99),
	ARCHER(Unit.TravelMedium.LAND, 25, true, new Unit.TravelMedium[]{Unit.TravelMedium.LAND,Unit.TravelMedium.SEA, Unit.TravelMedium.DRAGON}, 6, Unit.DamageType.ARROW, 2, 3, 3, Unit.ArmorType.LEATHER, 3, Unit.MovementType.FOOT, false, 2, 50, 99),
	CANNON(Unit.TravelMedium.LAND, 150, true, new Unit.TravelMedium[]{Unit.TravelMedium.LAND,Unit.TravelMedium.SEA,Unit.TravelMedium.AIR, Unit.TravelMedium.DRAGON}, 9, Unit.DamageType.CANNON, 1, 4, 1, Unit.ArmorType.NONE, 2, Unit.MovementType.WHEEL, false, 1, 35, 50),

	ZEPPELIN(Unit.TravelMedium.AIR, 130, true, new Unit.TravelMedium[]{Unit.TravelMedium.LAND,Unit.TravelMedium.SEA}, 10, Unit.DamageType.CANNON, 1, 5, 0, Unit.ArmorType.NONE, 4, Unit.MovementType.FLYING, false, 2, 35, 50),
	DRAGON(Unit.TravelMedium.DRAGON, 380, false, new Unit.TravelMedium[]{Unit.TravelMedium.LAND,Unit.TravelMedium.SEA,Unit.TravelMedium.AIR, Unit.TravelMedium.DRAGON}, 15, Unit.DamageType.DRAGON, 1, 1, 10, Unit.ArmorType.DRAGON, 4, Unit.MovementType.FLYING, false, 2, 25, 50),
	
	CORSAIR(Unit.TravelMedium.SEA, 130, true, new Unit.TravelMedium[]{Unit.TravelMedium.LAND,Unit.TravelMedium.SEA}, 7, Unit.DamageType.CANNON, 1, 4, 5, Unit.ArmorType.WOOD, 3, Unit.MovementType.WATER, false, 3, 50, 75),
	KRAKEN(Unit.TravelMedium.SEA, 300, false, new Unit.TravelMedium[]{Unit.TravelMedium.SEA}, 20, Unit.DamageType.KRAKEN, 1, 1, 15, Unit.ArmorType.KRAKEN, 3, Unit.MovementType.WATER, false, 1, 25, 50);

    public final Unit.TravelMedium travelMedium;

    public final int cost;
    public final int maxHealth = 10;

    public final int minRange;
    public final int maxRange;
    public final boolean isIndirect; //true->Indirect, false->Direct
    public final ArrayList<Unit.TravelMedium> validTargets;

    public final int damage;
    public final Unit.DamageType damageType;

    public final int armor;
    public final Unit.ArmorType armorType;

    public final int speed;
    public final Unit.MovementType movementType;

    public final Boolean canCapture;

    public final int mana; //Used for attacks
    public final int stamina; //Used for moving
    public final int vision;

	UnitType(Unit.TravelMedium travelMedium, int price, boolean isIndirect, Unit.TravelMedium[] validTargets, int damage, Unit.DamageType damageType, int minRange, int maxRange, int armor, Unit.ArmorType armorType, int speed, Unit.MovementType movementType, boolean canCapture, int vision, int mana, int stamina){
        
        this.travelMedium = travelMedium;

        this.cost = price;

        this.minRange = minRange;
        this.maxRange = maxRange;
        this.isIndirect = isIndirect;
        this.validTargets = new ArrayList<Unit.TravelMedium>(Arrays.asList(validTargets));

        this.damage = damage;
        this.damageType = damageType;

        this.armor = armor;
        this.armorType = armorType;

        this.speed = speed;
        this.movementType = movementType;

        this.canCapture = canCapture;

        this.mana = mana;
        this.stamina = stamina;
        this.vision = vision;
	}

	public Image getImage(Player.Team owner){
		switch(this){

            case SCOUT:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedScout.png"); 
                }
                else {
                   return new Image("res/images/BlueScout.png"); 
                }
            case SWORDSMEN:
                if (owner == Player.Team.RED) {

                    return new Image("res/images/RedSwordsman.png"); 
                }
                else {
                   return new Image("res/images/BlueSwordsman.png"); 
                }
            case KNIGHT:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedKnight.png"); 
                }
                else {
                   return new Image("res/images/BlueKnight.png"); 
                }
            case MAGE:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedMage.png"); 
                }
                else {
                   return new Image("res/images/BlueMage.png"); 
                }
            case ARCHER:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedArcher.png"); 
                }
                else {
                   return new Image("res/images/BlueArcher.png"); 
                }
            case CANNON:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedCannon.png"); 
                }
                else {
                   return new Image("res/images/BlueCannon.png"); 
                }
            case ZEPPELIN:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedZeppelin.png"); 
                }
                else {
                   return new Image("res/images/BlueZeppelin.png"); 
                }
            case DRAGON:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedDragon.png");
                }
                else {
                   return new Image("res/images/BlueDragon.png"); 
                }
            case CORSAIR:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedCorsair.png"); 
                }
                else {
                   return new Image("res/images/BlueCorsair.png"); 
                }
            case KRAKEN:
                if (owner == Player.Team.RED) {
                    return new Image("res/images/RedKraken.png"); 
                }
                else {
                   return new Image("res/images/BlueKraken.png"); 
                }
            
			default:
				return null;
		}
	}
    public String toString() {
        switch(this) {
            case SCOUT:
                return "Scout";
            case SWORDSMEN:
                return "Swordsmen";
            case KNIGHT:
                return "Knight";
            case MAGE:
                return "Mage";
            case ARCHER:
                return "Archer";
            case CANNON:
                return "Cannon";
            case ZEPPELIN:
                return "Zeppelin";
            case DRAGON:
                return "Dragon";
            case CORSAIR:
                return "Corsair";
            case KRAKEN:
                return "Kraken";
            default:
                return null;
        }
    }
}
