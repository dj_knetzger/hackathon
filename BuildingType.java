import java.util.ArrayList;
import javafx.scene.image.Image;

public enum BuildingType {
	CITY(new Unit.TravelMedium[]{}),
	BARRACKS(new Unit.TravelMedium[]{Unit.TravelMedium.LAND}),
	AVIARY(new Unit.TravelMedium[]{Unit.TravelMedium.AIR,Unit.TravelMedium.DRAGON}),
	PORT(new Unit.TravelMedium[]{Unit.TravelMedium.SEA});

	public final ArrayList<UnitType> unitList;
	public final int captureHP = 20;
	public final int income = 10;

	BuildingType(Unit.TravelMedium[] unitBuildType){
		this.unitList = new ArrayList<UnitType>();
		for(Unit.TravelMedium travelMedium : unitBuildType){
			this.unitList.addAll(Unit.getUnitsOfTravelMedium(travelMedium));
		}
	}

	public Image getImage(Player player){
		Player.Team team;
		if(player == null){
			team = null;
		} else {
			team = player.team;
		}
		switch(this){
			case CITY:
				if(team == null){
					return new Image("res/images/CityTile.png");
				}else if(team == Player.Team.RED){
					return new Image("res/images/RedCityTile.png");
				}else{
					return new Image("res/images/BlueCityTile.png");
				}
			case AVIARY:
				if(team == null){
					return new Image("res/images/AviaryTile.png");
				}else if(team == Player.Team.RED){
					return new Image("res/images/RedAviaryTile.png");
				}else{
					return new Image("res/images/BlueAviaryTile.png");
				}
			case BARRACKS:
				if(team == null){
					return new Image("res/images/BarracksTile.png");
				}else if(team == Player.Team.RED){
					return new Image("res/images/RedBarracksTile.png");
				}else{
					return new Image("res/images/BlueBarracksTile.png");
				}
			case PORT:
				if(team == null){
					return new Image("res/images/PortTile.png");
				}else if(team == Player.Team.RED){
					return new Image("res/images/RedPortTile.png");
				}else{
					return new Image("res/images/BluePortTile.png");
				}
			default:
				return null;
		}
	}
}