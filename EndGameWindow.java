import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.Control;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.geometry.Pos;

public class EndGameWindow extends Stage implements EventHandler<ActionEvent>{

	public EndGameWindow() {
		super();
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		
		Text text = new Text("Game over!");
		GridPane.setConstraints(text, 0, 0);
		grid.getChildren().add(text);

		Button b = new Button("Ok");
		b.setOnAction(this);
		GridPane.setConstraints(b, 0, 1);
		grid.getChildren().add(b);

		b.setMaxWidth(text.maxWidth(200));
		b.setMinWidth(text.minWidth(200));

		b.setAlignment(Pos.BOTTOM_CENTER);

		Scene scene = new Scene(grid, 200, 500);

		setTitle("Game over!");
		setMaxWidth(200);
		setMaxHeight(200);
		setMinWidth(200);
		setMinHeight(200);
		setScene(scene);
		show();
	}

	@Override
	public void handle(ActionEvent e){
		System.exit(0);
	}
}