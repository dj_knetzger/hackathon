import java.util.ArrayList;

public class Player{

	public static enum Team{ RED, BLUE}

	public final Team team;

	public int money;
	public ArrayList<Unit> units = new ArrayList<Unit>();
	public ArrayList<Building> buildings = new ArrayList<Building>();

	public boolean hasUnits;
	
	public Player(Team id){
		this.team = id;

		this.money = 5000;
		hasUnits = true;
	}

	public void addUnit(Unit unit){
		this.units.add(unit);
		unit.team = this.team;
		hasUnits = true;
	}

	public void removeUnit(Unit unit){
		this.units.remove(unit);
		if(units.size()==0){
			hasUnits = false;
		}
	}

	public void addBuilding(Building building){
		this.buildings.add(building);
		building.owner = this;
	}

	public void removeBuilding(Building building){
		this.buildings.remove(building);
		building.owner = null;
	}

	public boolean isBuildingOwned(Building building){
		return (building.owner.team==this.team && this.buildings.contains(building));
	}

	public int getProfit(){
		int profit = 0;
		for(Building building : buildings){
			profit += building.type.income;
		}
		return profit;
	}

	public boolean isTurnDone(){
		boolean done = true;
		for(Unit unit : units){
			done = done && unit.hasAttackedThisTurn;
		}
		for(Building building : buildings){
			done = done && building.hasBuiltThisTurn;
		}
		return done && !units.isEmpty();
	}

	public void endTurn(){
		for(Unit unit : units){
			unit.endTurn();
		}
		for(Building building : buildings){
			building.hasBuiltThisTurn = false;
		}
		this.money += getProfit();
	}
}