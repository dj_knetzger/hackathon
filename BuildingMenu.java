import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class BuildingMenu extends Stage implements EventHandler<ActionEvent>{

	private Tile sourceTile;
	private Player owner;

	public BuildingMenu(Tile tile, Player player) {
		super();
		GridPane grid = new GridPane();

		if (tile.terrain.toString() == "BARRACKS") {
			int x = 0;
			for(UnitType type : UnitType.values()) {
				if (type.travelMedium.toString() == "LAND") {

					Button button = new BuildingButton(type);

					if (type.cost > player.money) {
						button.setDisable(true);
					}

					GridPane.setConstraints(button, 0, x);
					grid.getChildren().add(button);
					x++;
					button.setOnAction(this);

				}
			}
			Scene scene = new Scene(grid, 200, 500);
			setTitle("Barracks");
			setMaxWidth(200);
			setMaxHeight(350);
			setMinWidth(200);
			setMinHeight(350);
			setScene(scene);
			show();
		}

		else if (tile.terrain.toString() == "AVIARY") {
			int x = 0;
			for(UnitType type : UnitType.values()) {
				if (type.travelMedium.toString() == "AIR" || type.travelMedium.toString() == "DRAGON" ) {

					Button button = new BuildingButton(type);

					if (type.cost > player.money) {
						button.setDisable(true);
					}

					GridPane.setConstraints(button, 0, x);
					grid.getChildren().add(button);
					x++;
					button.setOnAction(this);

				}
			}
			Scene scene = new Scene(grid, 200, 500);
			setTitle("Aviary");
			setMaxWidth(200);
			setMaxHeight(150);
			setMinWidth(200);
			setMinHeight(150);
			setScene(scene);
			show();	
		}

		else if (tile.terrain.toString() == "PORT") {
			int x = 0;
			for(UnitType type : UnitType.values()) {
				if (type.travelMedium.toString() == "SEA") {

					Button button = new BuildingButton(type);

					if (type.cost > player.money) {
						button.setDisable(true);
					}

					GridPane.setConstraints(button, 0, x);
					grid.getChildren().add(button);
					x++;
					button.setOnAction(this);

				}
			}
			Scene scene = new Scene(grid, 200, 500);
			setTitle("Port");
			setMaxWidth(200);
			setMaxHeight(150);
			setMinWidth(200);
			setMinHeight(150);
			setScene(scene);
			show();

		}

		else {
			return;
		}

		this.sourceTile = tile;
		this.owner = player;
	}

	@Override
	public void handle(ActionEvent e){
		UnitType type = ((BuildingButton)e.getSource()).type;
		if (type.cost <= owner.money) {
			if(type == UnitType.ZEPPELIN){
				sourceTile.addUnit(new Zeppelin(owner,sourceTile));
			}else{
				sourceTile.addUnit(new Unit(type,owner,sourceTile));
			}
			sourceTile.unit.hasMovedThisTurn = true;
			sourceTile.unit.hasAttackedThisTurn = true;
			owner.money -= ((BuildingButton)e.getSource()).type.cost;
			sourceTile.building.hasBuiltThisTurn = true;
			close();
			GridWorld.checkTurnEndStatically();
		}
	}

	private class BuildingButton extends Button{
		public UnitType type;
		public BuildingButton(UnitType unitType){
			super(unitType.toString() + " Cost: " + unitType.cost);

			setMaxWidth(200);
			setMaxHeight(50);
			setMinWidth(200);
			setMinHeight(50);

			this.type = unitType;
		}
	}
}