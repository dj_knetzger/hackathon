import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class GameWindow extends Application {
	private GridWorld gameGrid;
	private StackPane root;
	
	public static void main(String[] args) {
        launch(args);
    }

	@Override
	public void start(Stage primaryStage) throws Exception {
		root = new StackPane();
		Scene scene = new Scene(root, 960, 576);
		scene.setOnKeyTyped(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent e) {
				switch(e.getCharacter().toString()) {
				case "m":
					if(!gameGrid.moveflag && gameGrid.focusedTile != null && !gameGrid.focusedTile.unit.hasMovedThisTurn){
						for(Tile targetTile:gameGrid.targetTiles){
							targetTile.clearMask();
						}
						gameGrid.targetTiles = new ArrayList<Tile>();
						gameGrid.moveflag = true;
						gameGrid.attackflag = false;
						gameGrid.loadFlag = false;
						gameGrid.dropFlag = false;
						ArrayList<Tile> accessibleTiles = gameGrid.getAccessibleTiles(gameGrid.selectedTile, null);
						gameGrid.targetTiles = accessibleTiles;
						for(Tile accessibleTile:accessibleTiles){
							accessibleTile.setMask();
						}
					}
					break;
				case "a":
					if(!gameGrid.attackflag && gameGrid.focusedTile != null && !gameGrid.focusedTile.unit.hasAttackedThisTurn){
						for(Tile targetTile:gameGrid.targetTiles){
							targetTile.clearMask();
						}
						gameGrid.targetTiles = new ArrayList<Tile>();
						gameGrid.attackflag = true;
						gameGrid.moveflag = false;
						gameGrid.loadFlag = false;
						gameGrid.dropFlag = false;
						ArrayList<Tile> targetableTiles = gameGrid.getTargetableTiles(gameGrid.selectedTile);
						gameGrid.targetTiles = targetableTiles;
						for(Tile targetTile:targetableTiles){
							targetTile.setMask();
						}
					}
					break;
				case "w":
					if(gameGrid.focusedTile != null){
						gameGrid.focusedTile.unit.hasAttackedThisTurn = true;
						gameGrid.focusedTile.unit.hasMovedThisTurn = true;
						gameGrid.clearFocusedTile();
					}
					break;
				case "c":
					if(gameGrid.focusedTile != null 
							&& !gameGrid.focusedTile.unit.hasAttackedThisTurn 
							&& gameGrid.buildings.contains(gameGrid.focusedTile) 
							&& gameGrid.focusedTile.building.owner != gameGrid.focusedTile.unit.owner){
						boolean captured = gameGrid.focusedTile.building.captureBuilding(gameGrid.focusedTile.unit);
						gameGrid.focusedTile.unit.hasAttackedThisTurn = true;
						if(captured){
							gameGrid.focusedTile.getChildren().remove(gameGrid.focusedTile.terrainView);
							gameGrid.focusedTile.terrainView = new ImageView(gameGrid.focusedTile.building.getImage());
							gameGrid.focusedTile.getChildren().add(gameGrid.focusedTile.terrainView);
							gameGrid.focusedTile.terrainView.toBack();
						}
						gameGrid.clearFocusedTile();
					}
					break;
				case "p":
					gameGrid.clearFocusedTile();
					gameGrid.endTurn();
					break;
				case "l":
					if(gameGrid.focusedTile != null && !gameGrid.focusedTile.unit.hasAttackedThisTurn){
						for(Tile targetTile:gameGrid.targetTiles){
							targetTile.clearMask();
						}
						gameGrid.targetTiles = new ArrayList<Tile>();
						gameGrid.attackflag = false;
						gameGrid.moveflag = false;
						gameGrid.loadFlag = true;
						gameGrid.dropFlag = false;
						ArrayList<Tile> targetableTiles = gameGrid.getLoadableTiles(gameGrid.selectedTile);
						gameGrid.targetTiles = targetableTiles;
						for(Tile targetTile:targetableTiles){
							targetTile.setMask();
						}
					}
					break;
				case "d":
					if(gameGrid.focusedTile != null && gameGrid.focusedTile.unit.type==UnitType.ZEPPELIN && !gameGrid.focusedTile.unit.hasAttackedThisTurn){
						for(Tile targetTile:gameGrid.targetTiles){
							targetTile.clearMask();
						}
						gameGrid.targetTiles = new ArrayList<Tile>();
						gameGrid.attackflag = false;
						gameGrid.moveflag = false;
						gameGrid.loadFlag = false;
						gameGrid.dropFlag = true;
						ArrayList<Tile> targetableTiles = gameGrid.getAccessibleTiles(gameGrid.selectedTile, ((Zeppelin)gameGrid.focusedTile.unit).getLoadedUnit());
						gameGrid.targetTiles = targetableTiles;
						for(Tile targetTile:targetableTiles){
							targetTile.setMask();
						}
					}
					break;
				}
				gameGrid.checkTurnEnd();
			}
			
		});
		primaryStage.setTitle("Hackathon");
        primaryStage.setScene(scene);
        primaryStage.show();

        new InstructionWindow();

        GridPane grid = new GridPane();
        gameGrid = new GridWorld(grid, 20, 12, primaryStage);
        gameGrid.paintMap("res/maps/RiverMap.txt");

        root.getChildren().add(grid);
   	}	
}
