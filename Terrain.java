import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.image.Image;

public enum Terrain{
	MOUNTAIN(0.5, 0.25, new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.FLYING, Unit.MovementType.HORSE}),
	FIELD(1.0,1.0,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.WHEEL, Unit.MovementType.FLYING, Unit.MovementType.HORSE}),
	FOREST(0.75,0.5,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.FLYING, Unit.MovementType.HORSE, Unit.MovementType.WHEEL}),
	ROAD(1.25,1.25,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.FLYING, Unit.MovementType.HORSE, Unit.MovementType.WHEEL}),
	WATER(1.0,1.0,new Unit.MovementType[]{Unit.MovementType.FLYING, Unit.MovementType.WATER}),
	CITY(1.0,0.5,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.WHEEL, Unit.MovementType.FLYING, Unit.MovementType.HORSE}),
	BARRACKS(1.0,0.5,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.WHEEL, Unit.MovementType.FLYING, Unit.MovementType.HORSE}),
	AVIARY(1.0,0.5,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.WHEEL, Unit.MovementType.FLYING, Unit.MovementType.HORSE}),
	PORT(1.0,0.5,new Unit.MovementType[]{Unit.MovementType.FOOT, Unit.MovementType.WHEEL, Unit.MovementType.FLYING, Unit.MovementType.HORSE, Unit.MovementType.WATER});

	public final double movementModifier;
	public final double coverModifier;
	public final ArrayList<Unit.MovementType> allowedUnits;

	public Player owner;
	
	Terrain(double movementModifier, double coverModifier, Unit.MovementType[] allowedUnits){
		this.movementModifier = movementModifier;
		this.coverModifier = coverModifier;
		this.allowedUnits = new ArrayList<Unit.MovementType>(Arrays.asList(allowedUnits));
	}

	public boolean isLegalTerrain(Unit unit){
		return allowedUnits.contains(unit.type.movementType);
	}

	public boolean isBuilding(){
		switch(this){
			case CITY:
			case BARRACKS:
			case AVIARY:
			case PORT:
				return true;
			default:
				return false;
		}
	}

	public Image getImage(){
		switch(this){
			case MOUNTAIN:
				return new Image("res/images/MountainTile.png");
			case FIELD:
				return new Image("res/images/FieldTile.png");
			case FOREST:
				return new Image("res/images/ForestTile.png");
			case ROAD:
				return new Image("res/images/RoadTile.png");
			case WATER:
				return new Image("res/images/WaterTile.png");
			case CITY:
				return new Image("res/images/CityTile.png");
			case AVIARY:
				return new Image("res/images/AviaryTile.png");
			case BARRACKS:
				return new Image("res/images/BarracksTile.png");
			case PORT:
				return new Image("res/images/PortTile.png");
			default:
				return null;
		}
	}
}