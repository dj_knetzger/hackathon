import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class Tile extends StackPane{

	public Terrain terrain;
	public Building building = null; 
	public Unit unit;
	public int x;
	public int y;
	public ImageView terrainView;
	public ImageView unitImage;
	public ImageView mask;

	public Tile (int x, int y, Terrain terrain, Building building) {
		this.x = x;
		this.y = y;
		this.terrain = terrain;
		this.building = building;
		if(this.terrain.isBuilding()){
			this.terrainView = new ImageView(building.getImage());
		}else{
			this.terrainView = new ImageView(this.terrain.getImage());
		}
		this.getChildren().add(terrainView);
	}

	public void addUnit(Unit newUnit) {
		if(this.unit == null){
			this.unit = newUnit;
			this.unitImage = new ImageView(newUnit.getImage());
			this.getChildren().add(unitImage);
		}
	}

	public void removeUnit() {
		if(unit != null){
			this.getChildren().remove(unitImage);
			this.unit.removeAttributes();
			this.unit = null;
			unitImage = null;
		}
	}
	
	public void setMask(){
		this.mask = new ImageView(new Image("res/images/Target.png"));
		this.getChildren().add(mask);
	}
	
	public void clearMask(){
		this.getChildren().remove(mask);
		this.mask = null;
	}
	
	public void setSelected(){
		this.mask = new ImageView(new Image("res/images/Select.png"));
		this.getChildren().add(mask);
	}
	
	public void clearSelected(){
		this.getChildren().remove(mask);
		this.mask = null;
	}

	public int manDist(Tile target){
		return Math.abs(x-target.x)+Math.abs(y-target.y);
	}
}
