import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.paint.Color;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;

public class Unit {

    public UnitType type;
    public Player owner;
    public Player.Team team;
    public int hp,mana,stamina;
    public Tile curTile;

    public Text hpView;
    public Text manaView;
    public Text staminaView;

    public boolean hasMovedThisTurn;
    public boolean hasAttackedThisTurn;

    public boolean isLoaded;

    //Constructors
    public Unit(UnitType type, Player owner, Tile startTile){
        this.type = type;
        this.owner = owner;
        this.team = owner.team;

        this.hp = type.maxHealth;
        this.mana = type.mana;
        this.stamina = type.stamina;

        hpView = new Text(hp+"");
        hpView.setFont(new Font(14));
        hpView.setFill(Color.RED);

        manaView = new Text(mana+"");
        manaView.setFont(new Font(14));
        manaView.setFill(Color.BLUE);

        staminaView = new Text(stamina+"");
        staminaView.setFont(new Font(14));
        staminaView.setFill(Color.GREEN);

        move(startTile);
        owner.addUnit(this);

        hasMovedThisTurn = false;
        hasAttackedThisTurn = false;
    }

    public void move(Tile target){
        if(hasMovedThisTurn){
            return;
        }
        hasMovedThisTurn = true;
        if(curTile != null){
            curTile.removeUnit();
        }
        target.addUnit(this);
        curTile = target;
        displayAttributes();
    }

    public boolean canAttack(Unit target){
        return (!hasAttackedThisTurn) && (this.team != target.team) && (curTile.manDist(target.curTile) >= type.minRange && curTile.manDist(target.curTile) <= type.maxRange) && (type.validTargets.contains(target.type.travelMedium));
    }

    public boolean attack(Unit target, Terrain terrain){
        if(canAttack(target)){
            hasMovedThisTurn = true;
            hasAttackedThisTurn = true;
            int damage = calculateDamage(target, terrain);
            return target.takeDamage(damage, this, terrain);
        }
        return false;
    }

    public boolean counterAttack(Unit target, Terrain terrain) {
        if (canAttack(target)) {
            int damage = calculateDamage(target, terrain);
            return target.takeDamage(damage);
        }
        return false;
    }

    public int calculateDamage(Unit target, Terrain terrain){
        return (int)(type.damage * (hp/10.0) * (2.0/target.type.armor) * terrain.coverModifier);
    }

    public boolean takeDamage(int damage, Unit target, Terrain terrain){
        hp = Math.max(0,hp - damage);
        hpView.setText(hp+"");
        if(isDead()){
            if(type == UnitType.ZEPPELIN){
                Zeppelin z = (Zeppelin)this;
                Unit u = z.drop();
                owner.removeUnit(u);
            }
            owner.removeUnit(this);
            return true;
        }
        counterAttack(target, terrain);
        return false;
    }

    public boolean takeDamage(int damage){
        hp = Math.max(0,hp - damage);
        hpView.setText(hp+"");
        if(isDead()){
            if(type == UnitType.ZEPPELIN){
                Zeppelin z = (Zeppelin)this;
                Unit u = z.drop();
                owner.removeUnit(u);
            }
            owner.removeUnit(this);
            return true;
        }
        return false;
    }

    public boolean isDead(){
        return hp <= 0;
    }

    public void endTurn(){
        if(!isLoaded){
            hasMovedThisTurn = false;
            hasAttackedThisTurn = false;
        }
    }

    public int getCapturePower(){
        return hp;
    }

    public Image getImage(){
        return type.getImage(team);
    }

    public void displayAttributes(){
        curTile.getChildren().add(hpView);
        StackPane.setAlignment(hpView, Pos.BOTTOM_RIGHT);

        // curTile.getChildren().add(manaView);
        // StackPane.setAlignment(manaView, Pos.BOTTOM_LEFT);

        // curTile.getChildren().add(staminaView);
        // StackPane.setAlignment(staminaView, Pos.TOP_LEFT);
    }

    public void removeAttributes(){
        curTile.getChildren().remove(hpView);
        curTile.getChildren().remove(manaView);
        curTile.getChildren().remove(staminaView);
    }

    public static enum DamageType{
        BLADE, MAGIC, ARROW, CANNON, DRAGON, KRAKEN
    }

    public static enum ArmorType{
        NONE, LEATHER, PLATE, DRAGON, WOOD, KRAKEN
    }

    public static enum MovementType{
        FOOT, WHEEL, FLYING, WATER, HORSE
    }

    public static enum TravelMedium{
        LAND, AIR, SEA, DRAGON
    }

    public static ArrayList<UnitType> getUnitsOfTravelMedium(TravelMedium travelMedium){
        ArrayList<UnitType> units = new ArrayList<UnitType>();
        for(UnitType type : UnitType.values()){
            if(type.travelMedium == travelMedium){
                units.add(type);
            }
        }
        return units;
    }
}